

resource "random_pet" "name-staging" {}

resource "aws_security_group" "web-sg-staging" {
  name = "${random_pet.name-staging.id}-sg"
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


resource "aws_instance" "web-staging" {
  ami                         = "ami-0dba2cb6798deb6d8"
  instance_type               = "t2.micro"
  associate_public_ip_address = true
  key_name                    = "devops-esgi-2"
  vpc_security_group_ids      = [aws_security_group.web-sg-staging.id]
  tags = {
    Name = random_pet.name-staging.id
  }
  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = file("devops-esgi-2.pem")
      host        = aws_instance.web-staging.public_dns
    }
  }

  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu  -i ${aws_instance.web-staging.public_dns}, --private-key devops-esgi-2.pem site.yml"
  }

}




output "domain-name-staging" {
  value = aws_instance.web-staging.public_dns
}

