

provider "aws" {
  region = "us-east-1"
}

provider "random" {}

resource "random_pet" "name" {}

resource "aws_security_group" "web-sg" {
  name = "${random_pet.name.id}-sg"
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


resource "aws_instance" "web" {
  ami                         = "ami-0dba2cb6798deb6d8"
  instance_type               = "t2.micro"
  associate_public_ip_address = true
  key_name                    = "devops-esgi-2"
  vpc_security_group_ids      = [aws_security_group.web-sg.id]
  tags = {
    Name = random_pet.name.id
  }
  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = file("devops-esgi-2.pem")
      host        = aws_instance.web.public_dns
    }
  }


}




output "domain-name" {
  value = aws_instance.web.public_dns
}

